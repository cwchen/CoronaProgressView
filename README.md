# [Corona] ProgressView, Stepper, and Switch

A tiny demo program on ProgressView, Stepper, and Switch.

## Usage

Use [Git](https://git-scm.com/) to clone this repo:

```
$ git clone https://gitlab.com/cwchen/CoronaProgressView.git
```

Alternatively, download the compressed repo as a zip file.

Then, open *main.lua* with [Corona](https://coronalabs.com/product/).

## Copyright

2018, Michael Chen; Apache 2.0
