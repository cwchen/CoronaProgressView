-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

-- Load widget package.
local widget = require("widget")

-- Set background to beige.
display.setDefault("background", 245 / 255, 245 / 255, 220 / 255)

-- Init the internal counter of the progress view.
local progress = 0

-- Init the ProgressView object.
local progressView =
  widget.newProgressView(
    {
      x = display.contentWidth * 0.47,
      y = display.contentHeight * 0.36,
      width = display.contentWidth * 0.7,
      isAnimated = false
    }
  )

-- Set the initial progress for the ProgressView object.
progressView:setProgress(progress)

-- Init some parameters.
local stepper
local step = 0
local count = 0
local isRunning = true

-- Declare the listener to the progress Button object.
local function btnProgressListener(event)
  if isRunning and event.phase == "ended" then
    timer.performWithDelay(
      5,
      function()
        if count < 100 then
          count = count + 1
        end

        progress = count / 100
        progressView:setProgress(progress)

        if count % 10 == 0 then
          step = count / 10
          stepper:setValue(step)
        end
      end,
      101 - count
    )
  end
end

-- Init the progress Button object.
local btnProgress =
  widget.newButton(
    {
      left = display.contentWidth * 0.12,
      top = display.contentHeight * 0.44,
      label = "Run",
      isEnabled = true,
      onEvent = btnProgressListener,
      shape = "roundedRect",
      width = 100,
      labelColor = {
        default = {0 / 255, 0 / 255, 0 / 255},
        over = {0 / 255, 0 / 255, 0 / 255}
      },
      fillColor = {
        default = {180 / 255, 255 / 255, 255 / 255},
        over = {110 / 255, 255 / 255, 255 / 255}
      }
    }
  )

-- Declare the listener to the rollback Button object.
local function btnRollbackListener(event)
  if isRunning and event.phase == "ended" then
    timer.performWithDelay(
      5,
      function()
        if count > 0 then
          count = count - 1
        end

        progress = count / 100
        progressView:setProgress(progress)

        if count % 10 == 0 then
          step = count / 10
          stepper:setValue(step)
        end
      end,
      count + 1
    )
  end
end

-- Init the rollback Button object.
local btnRollback =
  widget.newButton(
    {
      left = display.contentWidth * 0.52,
      top = display.contentHeight * 0.44,
      label = "Back",
      isEnabled = true,
      onEvent = btnRollbackListener,
      shape = "roundedRect",
      width = 100,
      labelColor = {
        default = {0 / 255, 0 / 255, 0 / 255},
        over = {0 / 255, 0 / 255, 0 / 255}
      },
      fillColor = {
        default = {180 / 255, 255 / 255, 255 / 255},
        over = {110 / 255, 255 / 255, 255 / 255}
      }
    }
  )

-- Declare the listener to Stepper object.
local function onStepperPress(event)
  if isRunning then
    if event.phase == "increment" then
      step = step + 1
      count = (step / 10) * 100
      stepper:setValue(step)
      progress = step / 10
      progressView:setProgress(progress)
    elseif event.phase == "decrement" then
      step = step - 1
      count = (step / 10) * 100
      stepper:setValue(step)
      progress = step / 10
      progressView:setProgress(progress)
    elseif event.phase == "minLimit" then
    -- Pass.
    elseif event.phase == "maxLimit" then
    -- Pass.
    end
  end
end

-- Init the Stepper object.
stepper =
  widget.newStepper(
    {
      x = display.contentWidth * 0.35,
      y = display.contentHeight * 0.64,
      initialValue = step,
      minimumValue = 0,
      maximumValue = 10,
      onPress = onStepperPress
    }
  )

-- Declare the listener to on-off Switch object.
local function onOffPress(event)
  local switch = event.target

  if switch.isOn then
    isRunning = false
  else
    isRunning = true
  end
end

-- Init the on-off Switch object.
local onOff =
  widget.newSwitch(
    {
      x = display.contentWidth * 0.65,
      y = display.contentHeight * 0.64,
      initialSwitchState = true,
      onPress = onOffPress
    }
  )
