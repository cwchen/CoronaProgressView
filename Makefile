FORMATTER=luaformatter
FORMATTER_FLAGS=-a

CHECKER=luacheck
CHECKER_FLAGS=-g

SOURCES=main.lua


.PHONY: all format check

all: format check

format:
	$(FORMATTER) $(FORMATTER_FLAGS) $(SOURCES)

check:
	$(CHECKER) $(CHECKER_FLAGS) $(SOURCES)
